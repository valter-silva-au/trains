package com.esailors;

import com.esailors.exceptions.NoSuchRouteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

class DirectedGraph {
    private final HashMap<String, Town> towns = new HashMap<>();
    private final HashMap<String, LinkedList<Route>> routes = new HashMap<>();

    /**
     * Add a unique town into hashMap towns.
     * @param town
     */
    public void addTown(String town){
        if (!towns.containsKey(town)) {
            towns.put(town, new Town(town));
        }
    }

    /**
     * Return a town based on its ID.
     * @param id
     * @return Object Town inserted into 'towns' previously
     */
    public Town getTown(String id){
        return towns.get(id);
    }

    /**
     * Returns the hashMap 'towns'
     * @return HashMap containing all existing towns.
     */
    public HashMap<String, Town> getTowns() {
        return towns;
    }

    /**
     * Returns the hashMap 'routes'
     * @return HashMap containing all existing routes.
     */
    public HashMap<String, LinkedList<Route>> getRoutes(){
        return routes;
    }

    /**
     * Add a route between two towns.
     * @param source Town where the train departs
     * @param destination Town where the train arrives
     * @param distance The distance between the two towns
     */
    public void addRoute(String source, String destination, int distance){
        // ensures that the town exists
        addTown(source);
        addTown(destination);

        Town townSource = getTown(source);
        Town townDestination = getTown(destination);

        // initialize the list of neighbors
        if (routes.isEmpty() || routes.get(townSource.getId()) == null || routes.get(townSource.getId()).isEmpty()){
            routes.put(townSource.getId(), new LinkedList<>());
        }

        // add destination as a neighbor
        townSource.neighbors.add(townDestination);

        // specify the distance between the two towns
        routes.get(townSource.getId()).add(new Route(townDestination, distance));
    }

    /**
     * Check if there is a route between two town using breadth-first search.
     * @param source Starting town
     * @param destination Ending town
     * @return True if a route exist between the two towns, false otherwise
     */
    public boolean hasRouteBFS(String source, String destination){
        return hasRouteBFS(getTown(source), getTown(destination));
    }

    /**
     * Check if there is a route between two town using breadth-first search.
     * @param source Starting town
     * @param destination Ending town
     * @return True if a route exist between the two towns, false otherwise
     */
    private boolean hasRouteBFS(Town source, Town destination){
        LinkedList<String> visited = new LinkedList<>();
        return hasRouteBFS(source, destination, visited);
    }

    /**
     * Check if there is a route between two town using breadth-first search.
     * @param source Starting town
     * @param destination Ending town
     * @param visited List of town IDs already visited
     * @return True if a route exist between the two towns, false otherwise
     */
    private boolean hasRouteBFS(Town source, Town destination, LinkedList<String> visited){
        LinkedList<Town> nextToVisit = new LinkedList<>();

        // mark town as visited (avoid loop)
        nextToVisit.add(source);

        while(!nextToVisit.isEmpty()){
            // remove the first element from the queue
            Town town = nextToVisit.remove();
            if (town == destination){
                return true;
            }

            // if already visited, move to the loop iteration
            if (visited.contains(town.getId())){
                continue;
            }

            visited.add(town.getId());
            nextToVisit.addAll(town.neighbors);
        }

        return false;
    }

    /**
     * Check if there is a route between two town using deep-first search.
     * @param source Starting town
     * @param destination Ending town
     * @return True if a route exist between the two towns, false otherwise
     */
    public boolean hasRouteDFS(String source, String destination){
        return hasRouteDFS(getTown(source), getTown(destination));
    }

    /**
     * Check if there is a route between two town using deep-first search.
     * @param source Starting town
     * @param destination Ending town
     * @return True if a route exist between the two towns, false otherwise
     */
    private boolean hasRouteDFS(Town source, Town destination){
        LinkedList<String> visited = new LinkedList<>();
        return hasRouteDFS(source, destination, visited);
    }

    /**
     * Check if there is a route between two town using deep-first search.
     * @param source Starting town
     * @param destination Ending town
     * @param visited List of town IDs already visited
     * @return True if a route exist between the two towns, false otherwise
     */
    private boolean hasRouteDFS(Town source, Town destination, LinkedList<String> visited) {
        // there's no path, if node already visited
        if (visited.contains(source.getId())){
            return false;
        }

        visited.add(source.getId());
        if (source == destination){
            return true;
        }

        for (Town child : source.neighbors){
            if (hasRouteDFS(child, destination, visited)){
                return true;
            }
        }

        return false;
    }

    /**
     * Return the distance between two towns
     * @param source Starting town
     * @param destination Ending town
     * @return The distance between two towns
     * @throws NoSuchRouteException If a route doesn't exist between both town
     */
    private int getRouteDistance(String source, String destination) throws NoSuchRouteException {
        for (Route route : routes.get(source)) {
            if (route.getDestination().getId().equals(destination)) {
                return route.getDistance();
            }
        }
        throw new NoSuchRouteException();
    }

    /**
     * Calculate the distance of a route int the format "[A-E]-[A-E]".
     * You can specify as many routes you want. (Example: A-B-C-D and so on)
     * @param route Route which the distance will be calculated.
     * @return The total distance of the specified route.
     */
    public int calculateDistance(String route){
        int totalDistance = 0;
        String routes[] = route.split("-");
        for (int i = 0; i < routes.length-1; i++) {
            // ensure there is a route between both towns first
            if (hasRouteBFS(routes[i],routes[i + 1])){
                int distance = getRouteDistance(routes[i], routes[i + 1]);
                if (distance > 0) {
                    totalDistance += distance;
                }
            }
        }
        return totalDistance;
    }

    /**
     * Creates a route in a string format [A-Z]-[A-Z]
     * @param path List containing all the town ID to be combined.
     * @return Combined town IDs.
     */
    private String convertPathToRoute(LinkedList<String> path){
        StringBuilder route = new StringBuilder();
        for (String nodeId : path){
            route.append(nodeId).append("-");
        }

        // removes the last '-' character
        return route.substring(0, route.length()-1);
    }

    /**
     * The shortest route between two towns
     * @param source Starting town
     * @param destination Ending town
     * @return The shortest distance between two towns
     */
    public int shortestRouteFrom(String source, String destination){
        // TODO (valter) (improvement) : perhaps use BFS ?
        List<LinkedList<String>> paths = new ArrayList<>();

        if (source.equals(destination)) {
            // get all childs and calculate for each one of them
            for(Town child : getTown(source).neighbors){
                paths.addAll(getAllPaths(child.getId(), destination));
            }
        }else{
            paths = getAllPaths(source, destination);
        }

        int shortestDistance = Integer.MAX_VALUE;
        for (LinkedList<String> path : paths){
            // introduce source into the route again
            if (source.equals(destination)){
                path.addFirst(source);
            }

            String route = convertPathToRoute(path);
            int routeDistance = calculateDistance(route);
            if (routeDistance < shortestDistance){
                shortestDistance = routeDistance;
            }
        }

        return shortestDistance;
    }

    /**
     * List with all possible paths between two towns.
     * @param source Starting town
     * @param destination Ending town
     * @return List will all possible paths between two towns.
     */
    public List<LinkedList<String>> getAllPaths(String source, String destination){
        LinkedList<String> visited = new LinkedList<>();
        List<LinkedList<String>> paths = new ArrayList<>();
        getAllPaths(getTown(source), getTown(destination), visited, paths);

        return paths;
    }

    /**
     * Analyze all possible paths between source and destination
     * @param source Starting town
     * @param destination Ending town
     * @param visited List of visited towns
     * @param paths List of all possible paths founded
     */
    private void getAllPaths(Town source, Town destination, LinkedList<String> visited, List<LinkedList<String>> paths) {
        visited.add(source.getId());

        if (source == destination){
            paths.add(new LinkedList<>(visited));
        } else {
            for (Town child : source.neighbors) {
                if(!visited.contains(child.getId())) {
                    getAllPaths(child, destination, visited, paths);
                }
            }
        }
        // finishing the recursion, remove the last visited town
        visited.remove(source.getId());
    }

    /**
     * Calculate the number of trips from source to destination with the maximum maxStops
     * @param source Starting town
     * @param destination Ending town
     * @param maxStops Maximum stops allowed
     * @return Number of trips with maximum stops between source and destination
     */
    public int numberOfTripsWithMaximumStops(String source, String destination, int maxStops){
        int numberOfTrips = 0;
        List<LinkedList<String>> paths = new ArrayList<>();

        if (!source.equals(destination)) {
            paths = getAllPaths(source, destination);

        } else if (source.equals(destination)){
            for (Town child : getTown(source).neighbors){
                paths.addAll(getAllPaths(child.getId(), destination));
            }
        }

        for (LinkedList<String> path : paths){
            if (!source.equals(destination)) {
                // number of routes = vortexes - 1
                if (path.size()-1 <= maxStops) {
                    numberOfTrips++;
                }
            }else{
                // we already skipped the source when the source==destination
                if (path.size() <= maxStops) {
                    numberOfTrips++;
                }
            }
        }
        return numberOfTrips;
    }

    /**
     * Calculate the number of trips from source to destination with exact number of stops
     * @param source Starting town
     * @param destination Ending town
     * @param exactStops Exact stops allowed
     * @return Number of trips with exact stops between source and destination
     */
    public int numberOfTripsWithExactlyStops(String source, String destination, int exactStops){
        LinkedList<String> visited = new LinkedList<>();
        List<LinkedList<String>> paths = new ArrayList<>();
        numberOfTripsWithExactlyStops(getTown(source), getTown(destination), exactStops, visited, paths);

        return paths.size();
    }

    /**
     * Calculate the number of trips from source to destination with exact number of stops
     * @param source Starting town
     * @param destination Ending town
     * @param exactStops Exact stops allowed
     * @return Number of trips with exact stops between source and destination
     */
    private void numberOfTripsWithExactlyStops(Town source, Town destination, int exactStops, LinkedList<String> visited, List<LinkedList<String>> paths) {
        visited.add(source.getId());
        if (visited.size()-1 <= exactStops){
            if (source == destination && visited.size()-1 == exactStops){
                paths.add(new LinkedList<>(visited));
            } else {
                for (Town child : source.neighbors) {
                    numberOfTripsWithExactlyStops(child, destination, exactStops, visited, paths);
                }
            }
        }
        visited.removeLastOccurrence(source.getId());
    }

    /**
     * Returns the number of different route between two towns if their distance are less than <code>distance</code>
     * @param source Starting town
     * @param destination Ending town
     * @param distance The distance lower than
     * @return Number of different routes from <code>source</code> and <code>destination</code> with a distance lower than <code>distance</code>
     */
    public int numberOfDifferentRoutesWithADistanceLessThan(String source, String destination, int distance){
        LinkedList<String> visited = new LinkedList<>();
        List<LinkedList<String>> paths = new ArrayList<>();
        numberOfDifferentRoutesWithADistanceLessThan(getTown(source), getTown(destination), distance, visited, paths);
        if (source.equals(destination)) paths.remove(0);
        return paths.size();
    }

    /**
     * Returns the number of different route between two towns if their distance are less than <code>distance</code>
     * @param source Starting town
     * @param destination Ending town
     * @param distance The distance lower than
     * @return Number of different routes from <code>source</code> and <code>destination</code> with a distance lower than <code>distance</code>
     */
    private void numberOfDifferentRoutesWithADistanceLessThan(Town source, Town destination, int distance, LinkedList<String> visited, List<LinkedList<String>> paths){
        visited.add(source.getId());
        int currentDistance = calculateDistance(convertPathToRoute(visited));
        if (currentDistance < distance) {
            if (source.getId().equals(destination.getId())){
                paths.add(new LinkedList<>(visited));
            }
            for (Town child : source.neighbors) {
                numberOfDifferentRoutesWithADistanceLessThan(child, destination, distance, visited, paths);
            }
        }
        visited.removeLast();
    }

    /**
     * Prints the directed graph
     */
    public void print(){
        for (Map.Entry<String, LinkedList<Route>> route : getRoutes().entrySet()){
            StringBuilder routeAsString= new StringBuilder();
            for (Route r : route.getValue()){
                routeAsString.append(r.getDestination().getId()).append(",").append(r.getDistance()).append(" ");
            }
            System.out.println(route.getKey() + " --> " + routeAsString);
        }
    }

    /**
     * Print all paths from a list of paths
     * @param paths List of paths
     */
    private void printPaths(List<LinkedList<String>> paths){
        int count = 0;
        for (LinkedList<String> path : paths){
            System.out.print(count++ + ":");
            path.forEach(System.out::print);
            System.out.println();
        }
    }

}
