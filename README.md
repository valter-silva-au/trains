# Trains
---

You will need to clone this project first.

```
git clone git@bitbucket.org:valterhenrique/trains.git
```

Access the project's folder, and choose one of the following methods to run this project.

## How to run

Be sure to have maven >= 3.3.* and java 8 installed in your system.

    mvn test

## Authors

* **Valter Henrique** - [GitHub](https://github.com/valterhenrique)