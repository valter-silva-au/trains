package com.esailors;

import java.util.LinkedList;

public class Town {
    private final String id;
    final LinkedList<Town> neighbors = new LinkedList<>();

    public Town(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
