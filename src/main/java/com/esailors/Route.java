package com.esailors;

class Route {

    private final Town destination;
    private final int distance;

    public Route(Town destination, int distance) {
        this.destination = destination;
        this.distance = distance;
    }

    public Town getDestination() {
        return destination;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Route{" +
                "destination=" + destination +
                ", distance=" + distance +
                '}';
    }
}
