package com.esailors;

import com.esailors.exceptions.NoSuchRouteException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DirectedGraphTest {

    private DirectedGraph directedGraph;
    private String towns[] = {"A", "B", "C", "D", "E"};
    private String routes[] = {"AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"};

    @Test
    public void getTown() throws Exception {
        for (String town : towns) {
            Assert.assertNotNull(directedGraph.getTown(town));
            assertEquals(town, directedGraph.getTown(town).getId());
        }
    }

    @Test
    public void getTowns() throws Exception {
        Assert.assertNotNull(directedGraph.getTowns());
        assertEquals(towns.length, directedGraph.getTowns().size());
    }

    @Test
    public void getRoutes() throws Exception {
        Assert.assertNotNull(directedGraph.getRoutes());
        // we are using a town's ID on a hash map
        assertEquals(towns.length, directedGraph.getTowns().size());
    }

    @Before
    public void setUp() throws Exception {
        directedGraph = new DirectedGraph();

        // create routes between 2 towns (edges)
        for (String route: routes){
            String source = String.valueOf(route.toCharArray()[0]);
            String destination = String.valueOf(route.toCharArray()[1]);
            int distance = Integer.valueOf(String.valueOf(route.toCharArray()[2]));
            directedGraph.addRoute(source, destination, distance);
        }
    }

    @Test
    public void calculateDistance() throws Exception {
        assertEquals(9, directedGraph.calculateDistance("A-B-C")); // Output #1
        assertEquals(5, directedGraph.calculateDistance("A-D")); // Output #2
        assertEquals(13, directedGraph.calculateDistance("A-D-C")); // Output #3
        assertEquals(22, directedGraph.calculateDistance("A-E-B-C-D")); // Output #4
        assertEquals(7, directedGraph.calculateDistance("A-E"));
        assertEquals(23, directedGraph.calculateDistance("A-B-C-D-E"));
        assertEquals(18, directedGraph.calculateDistance("B-C-D-E"));
        assertEquals(14, directedGraph.calculateDistance("C-D-E"));
        assertEquals(6, directedGraph.calculateDistance("D-E"));
    }

    @Test(expected = NoSuchRouteException.class)
    public void noSuchRoute() {
        directedGraph.calculateDistance("A-E-D"); // Output #5
    }

    @Test
    public void numberOfTripsWithMaximumStops() throws Exception {
        assertEquals(2, directedGraph.numberOfTripsWithMaximumStops("C", "C", 3)); // Output #6
        assertEquals(4, directedGraph.numberOfTripsWithMaximumStops("A", "E", 3));
        assertEquals(2, directedGraph.numberOfTripsWithMaximumStops("B", "E", 3));
    }

    @Test
    public void numberOfTripsWithExactlyStops() throws Exception {
        assertEquals(3, directedGraph.numberOfTripsWithExactlyStops("A", "C", 4)); // Output #7
        assertEquals(3, directedGraph.numberOfTripsWithExactlyStops("A", "E", 4));
        assertEquals(1, directedGraph.numberOfTripsWithExactlyStops("B", "E", 3));
        assertEquals(1, directedGraph.numberOfTripsWithExactlyStops("C", "C", 3));
        assertEquals(1, directedGraph.numberOfTripsWithExactlyStops("B", "D", 4));
    }

    @Test
    public void shortestRouteFrom() throws Exception {
        assertEquals(9, directedGraph.shortestRouteFrom("A", "C")); // Output #8
        assertEquals(9, directedGraph.shortestRouteFrom("B", "B")); // Output #9
        assertEquals(2, directedGraph.shortestRouteFrom("C", "E"));
        assertEquals(7, directedGraph.shortestRouteFrom("A", "E"));
        assertEquals(5, directedGraph.shortestRouteFrom("A", "D"));
    }

    @Test
    public void numberOfDifferentRoutesWithADistanceLessThan() throws Exception {
        assertEquals(7, directedGraph.numberOfDifferentRoutesWithADistanceLessThan("C", "C", 30)); // Output #10
        assertEquals(7, directedGraph.numberOfDifferentRoutesWithADistanceLessThan("B", "C", 30));
        assertEquals(70, directedGraph.numberOfDifferentRoutesWithADistanceLessThan("A", "D", 60));
        assertEquals(162, directedGraph.numberOfDifferentRoutesWithADistanceLessThan("A", "E", 60));
    }

    @Test
    public void getAllPaths() throws Exception {
        assertEquals(2, directedGraph.getAllPaths("C", "B").size());
        assertEquals(5, directedGraph.getAllPaths("A", "E").size());
        assertEquals(2, directedGraph.getAllPaths("B", "E").size());
        assertEquals(1, directedGraph.getAllPaths("E", "D").size());
    }

    @Test
    public void hasRouteBFS() throws Exception {
        assertEquals(true, directedGraph.hasRouteBFS("C", "C"));
        assertEquals(true, directedGraph.hasRouteBFS("A", "E"));
        assertEquals(true, directedGraph.hasRouteBFS("B", "C"));
        assertEquals(true, directedGraph.hasRouteBFS("C", "D"));
        assertEquals(true, directedGraph.hasRouteBFS("D", "E"));
    }

    @Test
    public void hasRouteDFS() throws Exception {
        assertEquals(true,  directedGraph.hasRouteDFS("C", "C"));
        assertEquals(true,  directedGraph.hasRouteDFS("A", "E"));
        assertEquals(true,  directedGraph.hasRouteDFS("B", "C"));
        assertEquals(true,  directedGraph.hasRouteDFS("C", "D"));
        assertEquals(true,  directedGraph.hasRouteDFS("D", "E"));
    }

    @Test
    public void dontHasRouteBFS() throws Exception {
        assertEquals(false, directedGraph.hasRouteBFS("B", "A"));
        assertEquals(false, directedGraph.hasRouteBFS("C", "A"));
        assertEquals(false, directedGraph.hasRouteBFS("D", "A"));
        assertEquals(false, directedGraph.hasRouteBFS("E", "A"));
    }

    @Test
    public void dontHasRoutesDFS() throws Exception {
        assertEquals(false, directedGraph.hasRouteDFS("B", "A"));
        assertEquals(false, directedGraph.hasRouteDFS("C", "A"));
        assertEquals(false, directedGraph.hasRouteDFS("D", "A"));
        assertEquals(false, directedGraph.hasRouteDFS("E", "A"));
    }

}