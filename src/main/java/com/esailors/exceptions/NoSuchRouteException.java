package com.esailors.exceptions;

public class NoSuchRouteException extends RuntimeException {
    public NoSuchRouteException() {
        super("NO SUCH ROUTE");
    }
}
